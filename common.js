'use strict';

const n3 = require('n3'),
	{ isEmpty } = require('lodash');

const defaultPrefixes = {
	'eal': 'http://tech4comp/eal/',
	'rdfs': 'http://www.w3.org/2000/01/rdf-schema#',
	'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
	'dct': 'http://purl.org/dc/terms/',
	'xml': 'http://www.w3.org/2001/XMLSchema#',
	'dbp': 'http://dbpedia.org/ontology/',
	'owl': 'http://www.w3.org/2002/07/owl#'
};

module.exports = {
	IsValidJson: function(data) {
		try {
			if( !isEmpty(data)) {
				if (typeof data === 'object') {
					JSON.parse(JSON.stringify(data));
				} else if (typeof data === 'string'){
					JSON.parse(data);
				}
			} else
				throw 'empty';
		} catch (e) {
			return false;
		}
		return true;
	},

	parseRDF: async function(doc) {
		const parser = new n3.Parser();
		const [quads, prefixes] = await new Promise((resolve, reject) => {//eslint-disable-line no-unused-vars
			let quads = [];
			parser.parse(doc, (error, quad, prefixes) => {
				if (error)
					reject(error);
				else if (quad)
					quads.push(quad);
				else
					resolve([quads, prefixes]);
			});
		});
		return quads;
	},

	writeRDF: async function (quads, format = 'application/n-triples', prefixes = defaultPrefixes) {
		return new Promise((resolve, reject) => {
			let options = { prefixes: prefixes };
			if (format) options.format = format;
			const writer = new n3.Writer(options);
			quads.forEach((quad) => {
				writer.addQuad(quad);
			});
			writer.end((error, result) => {
				if(error !== null)
					reject(error);
				resolve(result);
			});
		});
	}
};
