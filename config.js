'use strict';

const { isEmpty } = require('lodash');

const domain = !isEmpty(process.env.VIRTUAL_HOST) ? process.env.VIRTUAL_HOST : 'localhost';
const port = !isEmpty(process.env.VIRTUAL_PORT) ? process.env.VIRTUAL_PORT : '3000';
const protocol = 'http';
const hostname = `${protocol}://${domain}${(port === '80' ? '' : ':' + port)}`;

console.info('Using ' + hostname + ' as hostname');

// Mongo DB
const MONGO_HOSTNAME = !isEmpty(process.env.MONGO_HOSTNAME) ? process.env.MONGO_HOSTNAME : 'localhost';
const MONGO_PORT = !isEmpty(process.env.MONGO_PORT) ? process.env.MONGO_PORT : '27017';
const MONGO_USERNAME=!isEmpty(process.env.MONGO_USERNAME) ? (process.env.MONGO_USERNAME) : 'admin';
const MONGO_PASSWORD=!isEmpty(process.env.MONGO_PASSWORD) ? (process.env.MONGO_PASSWORD) : 'admin';


console.info('Using ' + MONGO_HOSTNAME + ' as mongo hostname');

module.exports = {
	domain: domain,
	port: port,
	protocol: protocol,
	hostname: hostname,
	MONGO_HOSTNAME,
	MONGO_PORT,
	MONGO_USERNAME,
	MONGO_PASSWORD
};
