'use strict';

const LogController = require('./app/controllers/logs');
const PruefungController = require('./app/controllers/pruefung');

module.exports = [
	{
		method: 'GET',
		path: '/logs',
		config: LogController.getLogs
	},
	{
		method: 'POST',
		path: '/logs',
		config: LogController.createLog
	},
	{
		method: 'POST',
		path: '/pruefungsanalyse/{id}/comment',
		config: PruefungController.updateComment
	},
	{
		method: 'POST',
		path: '/pruefungsanalyse/{id}/updatelabel',
		config: PruefungController.updateLabel
	},
	{
		method: 'GET',
		path: '/pruefungsanalyse',
		config: PruefungController.getExamList
	},
	{
		method: 'GET',
		path: '/pruefungsanalyse/{id}/delete',
		config: PruefungController.deleteExam
	},
	{
		method: 'GET',
		path: '/pruefungsanalyse/{id}/activity',
		config: PruefungController.getExamActivity
	},
	{
		method: 'GET',
		path: '/pruefungsanalyse/{id}',
		config: PruefungController.getExam
	},
	{
		method: 'GET',
		path: '/pruefungsanalyse/{id}/pdf',
		config: PruefungController.generateExamPDF
	},
	{
		method: 'POST',
		path: '/pruefungsanalyse/uploadzip',
		config: PruefungController.analyseUploadedExam
	},
];
