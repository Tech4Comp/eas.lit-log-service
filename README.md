# EAs.LiT Log and Exam Analysis Service

This service provides a REST-API (JSON) to create and access log statements, collected and visualized by the EAs.LiT Frontend. Furthermore it provides a REST-API (JSON) to store and retrieve assessment results, as well as analyse these.
Everything is stored inside a Mongodb instance.

## How to get started
1. Clone the repository and via `git clone ...`.
2. Change into the directory and install all dependencies via `npm install`
3. Transpile and run the project via `npm start`. The started service is accessible via [http://localhost:3000](http://localhost:3000)

The service is now started in development mode. You may run `npm run lint` to lint all code via ESLint. Have a look at the package.json file for other available commands.

The service is configured to use mongodb at `http://localhost:27017`. This default value may be changed by using the environment variable `MONGO_HOSTNAME`, `MONGO_PORT`, e.g. `export MONGO_PORT=8082`. For further environment variables, see the file [docker-compose.yml](./docker-compose.yml).

As soon as the service is started, an OpenAPI documentation page (swagger) is created at `/documentation` that may be used to test and experiment with this service.

## Production Environment
Use the included dockerfile to build a docker image and the included docker-compose.yml files to start it up.

Look at the dockerfile and docker-compose.yml files to gain insights into manual deployment.

## Automatic Builds and Deployments
By including `[build]` to a commit message on the main branch, a GitLab CI pipeline is triggered and builds a docker image, which is published on GitLab. Afterwards, a deployment stage is triggered, which deploys this newly built image to the staging environment. Deployment may also be triggered on its own, by including `[deploy]` to a commit message.

## Development
The root directory contains all source code, as well as all configuration files. The easiest way to get around is to look at the routes.js file.
