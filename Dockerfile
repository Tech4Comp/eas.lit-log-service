# See https://github.com/rmeissn/tech4comp-dockerfiles for the base image description
FROM node:lts-alpine
LABEL maintainer="Roy Meissner <meissner@informatik.uni-leipzig.de>"

ARG BUILD_ENV=LOCAL
ENV APPLICATION_PORT=${APPLICATION_PORT:-80}

RUN apk add --no-cache python3 pixman-dev cairo-dev pango-dev make g++ && rm -rf /var/cache/apk/*

RUN mkdir /nodeApp
WORKDIR /nodeApp

# ---------------- #
#   Installation   #
# ---------------- #

COPY ./ ./
RUN if [ "$BUILD_ENV" = "CI" ] ; then npm prune --production ; else rm -R node_modules ; npm install --production ; fi

# ----------------- #
#   Configuration   #
# ----------------- #

# ----------- #
#   Cleanup   #
# ----------- #

# -------- #
#   Run!   #
# -------- #

CMD ["npm","start"]
