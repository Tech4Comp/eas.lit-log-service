'use strict';

module.exports = {
	requireUserInfo (request, h) {
		const { username, userId } = request.payload;

		if (!username || !userId) return h.response({
			message: 'not-authenticated'
		}).takeover().code(401);
		
		return true;
	}
};