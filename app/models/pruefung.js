'use strict';

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const PointDistribution = new Schema({
	reached: Number,
	studentId: String,
	resultId: String
});

const Option = new Schema({
	id: String,
	text: String,
	points: Number,
	amountSelected: Number
});

const Answer = new Schema({
	shuffled: String,
	options: [Option]
});


const Item = new Schema({
	metadata: {
		ilias_version: String,
		questiontype: String,
		author: String,
		additional_cont_edit: String,
		externalid: String,
		thumb_size: String,
		feedback_setting: String
	},
	comment: String,
	id: String,
	maxattempts: String,
	duration: String,
	questiontype: String,
	question_fi: String,
	totalReachedPoints: Number,
	totalSubmitedAnswers: Number,
	pointDistribution: [PointDistribution],
	maxPoints: Number,
	itemtext: String,
	avgPoints: Number,
	sequence: Number,
	easlitId: Number,
	answers: [Answer],
});

const Participant = new Schema({
	active_fi: String,
	pass: String,
	points: String,
	maxpoints: String,
	questioncount: String,
	answeredquestions: String,
	workingtime: String,
	tstamp: String,
	active_id: String,
	user_fi: String,
	anonymous_id: String,
	test_fi: String,
	lastindex: String,
	tries: String,
	submitted: String,
	submittimestamp: String,
	fullname: String,
	user_criteria: String,
	usr_id: String
});

const PruefungSchema = new Schema({
	itemTypes: Array,
	answers: Array,
	shortAnalysis: {
		studentAmount: Number,
		itemAmount: Number,
		abgabenAmount: Number,
		maxPoints: Number,
		totalPoints: Number,
		passedStudents: Number,
		failedStudents: Number,
		avgPoints: Number,
		amountSolvedItems: Number,
		testId: String,
		testLabel: String,
		testDuration: String
	},
	label: String,
	userInfo: {
		username: String,
		userId: String
	},
	projectId: String,
	uploadTime: {
		type: Date,
		default: Date.now
	},
	items: [Item],
	content: Object,
	pointDistribution: Object,
	participants: [Participant],
	comment: {
		type: String,
		default: ''
	},
	examId: String
}, { collection: 'pruefungen' });

module.exports = {
	PruefungSchema
};