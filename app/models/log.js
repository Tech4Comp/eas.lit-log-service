'use strict';

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const LogSchema = new Schema({
	userInfo: {
		username: {
			type: String
		},
		userId: {
			type: String
		}
	},
	projectId: {
		type: String
	},
	operation: {
		type: String,
		enum: ['created', 'comment-added', 'deleted', 'updated']
	},
	text: {
		type: String,
		default: ''
	},
	link: {
		type: String,
		default: ''
	},
	timestamp: {
		type: Date,
		default: Date.now
	},
	content: Object,
	examId: String
});

module.exports = {
	LogSchema
};