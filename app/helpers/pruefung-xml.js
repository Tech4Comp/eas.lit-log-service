'use strict';

const fs = require('fs');
const xml2js = require('xml2js');
const parser = new xml2js.Parser({ attrkey: 'attr'});

module.exports = {
	async xmlToObject(path) {
		const stringContent = fs.readFileSync(path, 'utf-8');
		return await parser.parseStringPromise(stringContent).catch((e) => {
			console.error('Error parsing one of the Exam XMl Files, ' + path);
			throw Error(e);
		});
	}
};