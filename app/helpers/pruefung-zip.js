'use strict';

const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const AdmZip = require('adm-zip');

module.exports = {
	async saveZip(zipFile) {
		const tempName = uuidv4();
		const tempFolderPath = `tmp/${tempName}`;
		const zipPath = `${tempFolderPath}/${tempName}.zip`;

		try {
			if (!fs.existsSync('tmp')) fs.mkdirSync('tmp');
			fs.mkdirSync(tempFolderPath);
			fs.writeFileSync(zipPath, zipFile);

			return {
				zipPath,
				tempName
			};
		} catch(e) {
			console.error('Error unzipping file', e);
			throw Error(e);
		}
	},

	async extractZip(zipPath, targetPath) {
		const file = fs.readFileSync(zipPath);
		const zip = new AdmZip(file);

		const fileNames = [];
		zip.getEntries().forEach((entry) => {
			if (!entry.isDirectory && entry.entryName.endsWith('.xml')) {
				fileNames.push(entry.entryName);
				zip.extractEntryTo(entry.entryName, targetPath, true, true);
			}
		});

		return fileNames;
	},

	async cleanupTmp() {
		fs.rmSync('tmp', { recursive: true });
	}
};