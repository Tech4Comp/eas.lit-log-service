
'use strict';

const Mongoose = require('mongoose');
const Log = Mongoose.model('Log');

exports.getLogs = {
	tags: ['api', 'logs'],
	description: 'Returns Logs',
	handler: async (request, h) => {
		const d = new Date();
		d.setDate(d.getDate() - request.query.dayrange);
		const timerange = request.query.dayrange ? { '$gte': d } : null;

		const mongoQuery = {
			...request.query,
		};

		if (timerange) mongoQuery.timestamp = timerange;
		if (request.query.examId) mongoQuery.examId = request.query.examId.id;

		const logs = await Log.find(mongoQuery).sort({ timestamp: -1 }).limit(20).skip(((request.query.page || 1) - 1) * 20).catch((e) => {
			console.error(e);
			return h.code(500);
		});

    
		return h.response({
			logs
		}).code(200);
	}
};

exports.createLog = {
	tags: ['api', 'logs'],
	description: 'Creates a new Log Entry',
	handler: async (request, h) => {
		const log = {
			...request.payload
		};

		const doc = await Log.create(log).catch((error) => {
			console.log(error);
			return h.response().code(500);
		});

		return h.response({
			doc
		}).code(200);
	}
};