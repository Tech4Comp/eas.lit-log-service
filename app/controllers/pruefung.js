'use strict';

const { uniqueId } = require('lodash');
const Mongoose = require('mongoose');
const { Analyzer } = require('../analyzer/Analyzer');
const { cleanupTmp } = require('../helpers/pruefung-zip');
const { requireUserInfo } = require('../middleware/auth');
const Pruefung = Mongoose.model('Pruefung');
const Log = Mongoose.model('Log');

exports.getExam = {
	tags: ['api', 'pruefung'],
	description: 'Returns an Exam',
	handler: async (request, h) => {
		const document = await Pruefung.findOne({ examId: request.params.id }).catch((e) => {
			console.error(e);
			return h.response().code(500);
		});

		if (!document) return h.response().code(404);
		
		// Content can end up in a big download size if light is passed do not return it
		if (request.query.light) document.content = null;

		return h.response(document).code(200);
	}
};

exports.deleteExam = {
	tags: ['api', 'pruefung'],
	description: 'Deletes an Exam',
	handler: async (request, h) => {
		const deletedRes = await Pruefung.deleteOne({ examId: request.params.id }).catch((e) => {
			console.error(e);
			return h.response().code(500);            
		});

		return h.response(deletedRes).code(200);
	}
};

exports.getExamList = {
	tags: ['api', 'pruefung'],
	description: 'Returns a List of Exams',
	handler: async (request, h) => {
		const projectId = request.query.projectId;
		const page = request.query.page || 1;
		
		const documents = await Pruefung.find({ projectId }).sort({ uploadTime: -1 }).limit(10).skip((page - 1) * 10).catch((e) => {
			console.error(e);
			return h.response().code(500);
		});

		// Do not return content to save bandwidth if asked for by the client
		if (request.query.includeContent) documents.map((entry) => entry.content = null);

		return h.response(documents).code(200);
	}
};

exports.getExamActivity = {
	tags: ['api', 'pruefung'],
	description: 'Returns the log activity related to a specific Exam',
	handler: async (request, h) => {
		const relatedLogs = await Log.find({ examId: request.params.id }).sort({ timestamp: -1 }).limit(Number(request.query.limit) || 100).catch((e) => {
			console.error(e);
			return h.response().code(500);
		});

		return h.response(relatedLogs).code(200);
	}
};

exports.updateComment = {
	tags: ['api', 'pruefung'],
	description: 'Updated the Comment Field of a specific Exam',
	handler: async (request, h) => {
		await Pruefung.updateOne({ examId: request.params.id }, { comment: request.payload.comment}).catch((e) => {
			console.error(e);
			return h.response().code(500);
		});

		return h.response().code(200);
	}
};

exports.generateExamPDF = {
	tags: ['api', 'pruefung'],
	description: 'Generates a general Result PDF for the specific Exam',
	handler: async (request, h) => {
		const exam = await Pruefung.findOne({ examId: request.params.id });

		const analyser = new Analyzer();
		analyser.initWithObject(exam);

		await analyser.generateGeneralExamPDF();

		return 'done';
	}
};

exports.updateLabel = {
	tags: ['api', 'pruefung'],
	description: 'Updates the properties of an exam',
	handler: async (request, h) => {
		await Pruefung.updateOne({ examId: request.params.id }, { label: request.payload.label }).catch((e) => {
			console.log(e);
			return h.response().code(500);
		});

		return h.response().code(200);
	}
};

exports.analyseUploadedExam = {
	tags: ['api', 'pruefung'],
	description: 'Takes a .zip File -> extracts and analyze its contents',
	pre: [ { method: requireUserInfo }],
	payload: {
		// 10 MB
		maxBytes: 1000 * 1000 * 10,
		parse: true,
		multipart: true,
		output: 'data',
		allow: 'multipart/form-data'
	},
	handler: async (request, h) => {
		const zipFile = request.payload.file;

		const analyser = new Analyzer();
		await analyser.initWithZip(zipFile).catch((e) => console.error(e));
		
		const analyzedExam = analyser.exam;

		console.log(analyser.filesToAnalyse);
		console.log((analyzedExam.shortAnalysis.testId + '_' + request.payload.projectId));


		try {
			const examId = (analyzedExam.shortAnalysis.testId + '_' + request.payload.projectId) || uniqueId('exam_');
			await Pruefung.create({
				...analyzedExam,
				userInfo: {
					username: request.payload.username,
					userId: request.payload.userId
				},
				projectId: request.payload.projectId,
				examId
			});

			await cleanupTmp();

			return h.response({
				id: examId
			}).code(200);
		} catch (e) {
			await cleanupTmp();
	
			// Index Error
			if (e.code === 11000) {
				return h.response({
					code: 11000,
					message: 'duplicate-exam',
					examId: analyzedExam.shortAnalysis.testId
				}).code(400);
			}
			return h.response().code(500);
		}
	}
};