'use strict';

function shortAnalyzeResultsFile(fileContent) {
	const shortAnalysis = {
		studentAmount: 0,
		itemAmount: 0,
		abgabenAmount: 0,
		maxPoints: 0,
		totalPoints: 0,
		passedStudents: 0,
		failedStudents: 0,
		avgPoints: 0,
		amountSolvedItems: 0,
	};


	shortAnalysis.studentAmount = fileContent.tst_active[0].row.length;
	shortAnalysis.itemAmount = fileContent.tst_test_question ? fileContent.tst_test_question[0].row.length : 0;
	shortAnalysis.abgabenAmount = fileContent.tst_pass_result[0].row.length;

	const resultCache = fileContent.tst_result_cache[0].row;
	shortAnalysis.maxPoints = resultCache[0].attr.max_points;

	resultCache.forEach((entry) => {
		shortAnalysis.totalPoints += Number(entry.attr.reached_points);
		Number(entry.attr.passed) ? shortAnalysis.passedStudents++ : shortAnalysis.failedStudents++;
	});

	shortAnalysis.avgPoints = shortAnalysis.totalPoints / resultCache.length;

	shortAnalysis.amountSolvedItems = fileContent.tst_test_result[0].row.length;

	return shortAnalysis;
}

function analyzeResultsFile(fileContent, exam) {
	const items = exam.items;

	fileContent.tst_test_result[0].row.forEach((entry) => {
		const itemIndex = items.findIndex((item) => item.question_fi === entry.attr.question_fi);
		if (itemIndex !== -1) {
			items[itemIndex].pointDistribution.push({
				reached: Number(entry.attr.points),
				studentId: entry.attr.active_fi,
				resultId: entry.attr.test_result_id,
			});

			items[itemIndex].totalReachedPoints += Number(entry.attr.points);
			items[itemIndex].totalSubmitedAnswers += 1;
		}
	});

	items.forEach((item) => {
		item.avgPoints = item.totalReachedPoints / item.totalSubmitedAnswers;
	});

	if (fileContent.tst_test_question) {
		fileContent.tst_test_question[0].row.forEach((entry) => {
			const itemIndex = items.findIndex((item) => item.question_fi === entry.attr.question_fi);
			if (itemIndex !== -1) {
				items[itemIndex].sequence = Number(entry.attr.sequence);
			}
		});		
	}



	// Participants
	const participants = [];
	fileContent.tst_active[0].row.forEach((entry) => {
		const student = entry.attr;
		participants.push({
			...student,
		});
	});

	fileContent.tst_pass_result[0].row.forEach((entry) => {
		for (let x = 0; x < participants.length; x++) {
			if (participants[x].active_id === entry.attr.active_fi) {
				participants[x] = {
					...entry.attr,
					...participants[x],
				};
			}
		}
	});

	return {
		items,
		participants,
	};
}

function addAmountOfChosenPerItem(items, solutions) {
	solutions.forEach((solution) => {
		const data = solution.attr;

		const relatedItemIndex = items.findIndex((item) => item.question_fi === data.question_fi);

		if (relatedItemIndex !== -1) {
			if (items[relatedItemIndex].answers.options[Number(data.value1)]) items[relatedItemIndex].answers.options[Number(data.value1)].amountSelected += 1;
		}
	});

	return items;
}

function getAllAnswers(items) {
	const answers = [];

	items.forEach((item) => {
		item.answers.options.forEach((option) => {
			answers.push({
				question_fi: item.question_fi,
				question_id: item.id,
				...option,
			});
		});
	});

	return answers;
}

module.exports = {
	ResultsAnalyzer(data, exam) {
		const shortAnalysis = shortAnalyzeResultsFile(data.results);
		let items = exam.items;
		const analyzedResultData = analyzeResultsFile(data.results, exam);

		items = addAmountOfChosenPerItem(items, data.results.tst_solutions[0].row);

		const answers = getAllAnswers(items);

		return {
			shortAnalysis: shortAnalysis,
			items: items,
			participants: analyzedResultData.participants,
			pointDistribution: analyzedResultData.pointDistribution,
			answers: answers,
		};
	},
};
