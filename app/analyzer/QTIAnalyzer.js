'use strict';

function getItemIdFromComment(comment) {
	// Item ID Format: "EALID:http://item.easlit.erzw.uni-leipzig.de/item/{itemIdNumber}"
	if (!comment.startsWith('[EALID')) return null;

	const splitted = comment.split('/');
	return splitted[splitted.length - 1].replace(/\D/g,'');
}

function analyzeItems(items) {
	const formatedItems = [];

		
	items.forEach((item) => {
		const tmp = {
			metadata: {}
		};

		tmp.comment = item.qticomment[0];
		tmp.easlitId = getItemIdFromComment(item.qticomment[0]);
		tmp.id = item.attr.ident;
		tmp.maxattempts = item.attr.maxattempts;
		tmp.duration = item.duration[0];


		const metadata = item.itemmetadata[0].qtimetadata[0].qtimetadatafield;
		metadata.forEach((entry) => {
			tmp.metadata[entry.fieldlabel[0].toLowerCase()] = entry.fieldentry[0];
		});

		// replace spaces with _
		tmp.questiontype = tmp.metadata.questiontype.toLowerCase().replace(/ /g, '_');

		const n = tmp.id.lastIndexOf('_');
		tmp.question_fi = tmp.id.substring(n + 1);
		tmp.totalReachedPoints = 0;
		tmp.totalSubmitedAnswers = 0;
		tmp.pointDistribution = [];


		const flow = item.presentation[0].flow[0];

		// get max points
		let maxpoints = 0;

		const responses = flow.response_lid ? flow.response_lid[0].render_choice : null;
		const answers = {
			shuffled: null,
			options: []
		};

		if (responses && tmp.questiontype === 'single_choice_question') {
			answers.shuffled = responses[0].attr.shuffle;
			responses[0].response_label.forEach((option) => {
				let temp = {};

				temp.id = option.attr.ident;
				temp.text = option.material[0].mattext[0]._;
				temp.amountSelected = 0;


				answers.options.push(temp);
			});		
		}

		if (answers.shuffled !== null) {
			item.resprocessing[0].respcondition.forEach((condition, index) => {
				if (tmp.questiontype === 'single_choice_question') {
					const points = condition.setvar[0]._;
					if (points > maxpoints) maxpoints = Number(points);
						
					if (answers.options[index]) answers.options[index].points = Number(points);
				} else if (tmp.questiontype === 'multiple_choice_question' || tmp.questiontype === 'matching_question' || tmp.questiontype === 'ordering_question') {
					const points = condition.setvar[0]._;
					maxpoints += Number(points);
				} else if (tmp.questiontype === 'text_question') {
					maxpoints = Number(item.resprocessing[0].outcomes[0].decvar[0].attr.maxvalue);
				}
			});
			tmp.maxPoints = maxpoints || null;

			tmp.itemtext = flow.material[0].mattext[0]._;

			tmp.answers = answers;

			formatedItems.push(tmp);				
		}        
	});

	return formatedItems;
}

module.exports = {
	QTIAnalyzer(data, exam) {
		if (data.questestinterop.assessment) {
			const fileContent = data.questestinterop.assessment[0];
			const shortAnalysis = {
				testId: '',
				testLabel: '',
				testDuration: '',
			};

			shortAnalysis.testId = fileContent.attr.ident;
			shortAnalysis.testLabel = fileContent.attr.title;
			
			if (fileContent.duration) shortAnalysis.testDuration = fileContent.duration[0];
            
			const items = analyzeItems(fileContent.section[0].item);

			return {
				shortAnalysis: shortAnalysis,
				items
			};
		} else {
			const items = analyzeItems(data.questestinterop.item);

			return { items };
		}

	}
};