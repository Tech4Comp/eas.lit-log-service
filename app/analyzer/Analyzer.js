'use strict';

const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const AdmZip = require('adm-zip');
const { xmlToObject } = require('../helpers/pruefung-xml');
const { QTIAnalyzer } = require('./QTIAnalyzer');
const { ResultsAnalyzer } = require('./ResultsAnalyzer');
const PDFDocument = require('pdfkit');
const { ChartJSNodeCanvas } = require('chartjs-node-canvas');

function merge(source, target) {
	for (const [key, val] of Object.entries(source)) {
		if (val !== null && typeof val === 'object') {
			if (target[key] === undefined) {
				target[key] = new val.__proto__.constructor();
			}
			merge(val, target[key]);
		} else {
			target[key] = val;
		}
	}
	return target;
}


// order note - some files depend on each other, this is why we set a order
// for special cases the required files get processed first
const formatIndicators = [
	{
		filetype: 'qti',
		indicator: 'questestinterop',
		analyser: QTIAnalyzer,
		order: 1
	},
	{
		filetype: 'results',
		indicator: 'results',
		analyser: ResultsAnalyzer,
		order: 2
	}
];

class Analyzer {

	constructor() {
		this.filesToAnalyse = [];
		this.fileNames = [];
		this.exam = {
			answers: [],
			itemTypes: [],
			shortAnalysis: {
				studentAmount: 0,
				itemAmount: 0,
				abgabenAmount: 0,
				maxPoints: 0,
				totalPoints: 0,
				passedStudents: 0,
				failedStudents: 0,
				avgPoints: 0,
				amountSolvedItems: 0,
				testId: '',
				testLabel: '',
				testDuration: ''
			},
			label: '',
			items: [],
			content: {},
			participants: [],
			comment: '',
			examId: ''
		};
	}
    
	async initWithZip(zipFile) {
		const tempName = uuidv4();
		const tempFolderPath = `tmp/${tempName}`;
		const zipPath = `${tempFolderPath}/${tempName}.zip`;

		if (!fs.existsSync('tmp')) fs.mkdirSync('tmp');
		fs.mkdirSync(tempFolderPath);
		fs.writeFileSync(zipPath, zipFile);
        
		const file = fs.readFileSync(zipPath);
		const zip = new AdmZip(file);

		const fileNames = [];
		const zipEntries = zip.getEntries();

		for (const entry of zipEntries) {
			if (!entry.isDirectory && entry.entryName.endsWith('.xml')) {
				fileNames.push(entry.entryName);
				zip.extractEntryTo(entry.entryName, `tmp/${tempName}`, true, true);
			}
		}
			
		this.fileNames = fileNames;
        
		const filesToAnalyse = [];
		for (const fileName of fileNames) {
			const filePath = `tmp/${tempName}/${fileName}`;
			const fileContent = await xmlToObject(filePath);
			
			let found = false;
			formatIndicators.forEach((indicator) => {
				if (fileContent[indicator.indicator]) {
					filesToAnalyse.push({
						filetype: indicator.filetype,
						filePath,
						fileName,
						fileContent,
						analyzer: indicator.analyser,
						order: indicator.order
					});
					found = true;
				}
			});
            
			if (!found) filesToAnalyse.push({
				filetype: null,
				fileName,
				filePath,
			});
		}
        
		this.filesToAnalyse = filesToAnalyse.filter((e) => e.analyzer);
		this.filesToAnalyse.sort((a, b) => a.order - b.order);
		console.log(this.filesToAnalyse);
		this.analyseKnownFiles();
	}

	initWithObject(examObject) {
		this.exam = examObject;
	}

	async generateGeneralExamPDF() {
		await new Promise((resolve, reject) => {
			const doc = new PDFDocument;
			const writeStream = fs.createWriteStream('./test.pdf');
			doc.pipe(writeStream);


			// Titel
			doc.font('Helvetica-Bold').fontSize(14).text('Prüfungsergebnis ' + this.exam.shortAnalysis.testLabel);

			doc.moveDown(3);

			doc.circle(100, 50, 50)
				.lineWidth(3)
				.fillOpacity(0.8)
				.fillAndStroke('red', '#900');


			doc.end();

			writeStream.on('finish', () => {
				resolve();
			});				


		});

	}

	analyseKnownFiles() {
		this.filesToAnalyse.forEach((file) => {
			if (file.analyzer) {
				console.log('Analyzing with ', file.analyzer);
				const result = file.analyzer(file.fileContent, this.exam);
				this.exam = merge(result, this.exam);
			}
		});
	}
    

}

module.exports = {
	Analyzer
};